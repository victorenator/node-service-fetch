import {resolveSrv} from 'node:dns/promises';

/**
 * @typedef {object} SRV
 * @property {string} name
 * @property {number} port
 * @property {number} priority
 * @property {number} weight
 */

/**
 * @typedef {object} RequestService
 * @property {string} [domain] local.
 * @property {string} [type] _http._tcp
 */

/**
 * @param {string | URL} input
 * @param {RequestInit & RequestService} init
 * @returns {Promise<Response>}
 */
export default async function (input, init = {}) {
    const url = new URL(input);
    const domain = init.domain ?? 'local.';
    const type = init.type ?? '_http._tcp';
    /** @type {SRV[]} */
    const services = await resolveSrv(`${url.hostname}.${type}.${domain}`);
    services.sort(compareServicesPriority);

    for (const service of services) {
        try {
            url.hostname = service.name;
            url.port = service.port;
            return await fetch(url, init);

        } catch {
            continue;
        }
    }

    throw new Error('Service not available');
}

/**
 * Compares services by priority
 * @param {SRV} srv1
 * @param {SRV} srv2
 * @returns {number}
 */
function compareServicesPriority(srv1, srv2) {
    return srv2.priority - srv1.priority;
}
