# Feach API with dnssd services #

## License ##
MIT


## Install ##
```bash
npm install @victorenator/service-fetch
```


## Usage ##
```js
import fetch from '@victorenator/service-fetch';

await fetch('http://service-name/path');
```
